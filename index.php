<?php
    include('vendor/autoload.php');
    use cal\Calculator;

    if($_SERVER['REQUEST_METHOD']=="POST"){
        // echo "<pre>";
        // echo "</pre>";
            $numberOne = ($_POST['number_1']);
            $numberTwo = ($_POST['number_2']);
            $opr = ($_POST['operation']);

            $calculator = new Calculator;

            // $calculate->sum($numberOne,$numberTwo);
            // Eta na kore amake prothome check korte hobe- operator ta ke?

            if($opr == "plus"){
                $output = $calculator->sum($numberOne,$numberTwo);
            }
            elseif($opr == "minus"){
                $output = $calculator->sub($numberOne,$numberTwo);
            }
            elseif($opr == "multiplication"){
                $calculator->mul($numberOne,$numberTwo);
            }
            elseif($opr == "division"){
                $output = $calculator->div($numberOne,$numberTwo);
            }
            else{
                $output = $calculator->mod($numberOne,$numberTwo);
            }
    }

?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Calculator</title>
  </head>
  <body>
    <h1 class="text-center m-3">Calculator Design</h1>

    <div class="container d-flex justify-content-center align-items-center" style="background-color:#ddd; height:500px;"> <!--d-flex justify-content-center align-items-center-->
                    <div class="card w-50 shadow-lg p-3 mb-5 bg-light mx-auto" style="border-radius:20px;">
                            <div class="card-body">
                                <form action="" method="POST">
                                <div class="mb-3">
                                    <label for="number_1" class="form-label">Number One</label>
                                    <input type="number" 
                                        class="form-control" 
                                        id="number_1" 
                                        placeholder="enter the first number"
                                        name="number_1"
                                    >
                                    <!-- <--!form theke je data receive korbo, she ei name deye dhorbe> -->
                                    </div>

                                    <div class="mb-3">
                                    <label for="number_2" class="form-label">Number Two</label>
                                    <input 
                                        type="number" 
                                        class="form-control" 
                                        id="number_2" 
                                        placeholder="enter the second number"
                                         name="number_2"
                                    >
                                    </div>

                                    <label for="operation" class="mb-2">Choose Operation</label>
                                    <select name="operation" id="operation" class="form-select" name="operation">
                                                            <option value="s_one">Select One</option>
                                                            <option value="plus">+</option>
                                                            <option value="minus">-</option>
                                                            <option value="multiplication">*</option>
                                                            <option value="division">/</option>
                                                            <option value="modulus">%</option>
                                    </select>

                                    <a href="" type="submit" class="btn btn-sm btn-outline-success mt-3">Calculate</a>
                                </form>
                            </div>
                    </div>
    </div>

    
    













    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>