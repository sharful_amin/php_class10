<?php


    include('vendor/autoload.php');
    use cal\Parents;
    use cal\Sibling;
    use cal\Student;

    $pr = new Parents();
    echo $pr->house_name;

    // echo $pr->location;
    // private property gulo ke kokhono class er bahire access korte parbo na.

    echo $pr->displayLocation();
    // mane, private property gulo ke shudhu class er vhitore access kora jabe, jode class er bahire kono jaygay access korte hoy, tahole take kono na kono public method e access kore, tarpor take call deye access korte hobe (fhelte hobe).

    // echo $pr->amount;
    // ei khetre oo error show korbe, karan protected property ke oo ame class bahire access korte parbo na. Shudhu matro public property chara onno kono property ke class er bahire access korte parbo na.

    // echo $pr->displayAmount();

    $sibling = new Sibling();

    $sibling->display();
    /*
        Kono property jode protected hoy- take abosshoi class er bahire access kora jabe na. Kentu take jode kono class inherit kore, tar moddhe ame take access korte parbo. Mane ei protected er area hocche- she je class er property oi class, oi class ke kew jode abar extend kore oi class er oo she property hoye jabe.
    */

    echo $pr->displayAmount();

    // $student = new Student();
    // $student->studentInfo();

    Student::studentInfo();
    // static property ke class er bahire escope regulation deye access kora jabe, r class er vhitore hole tokhon shudhu self keyword deye access kora jabe and non-static hole take @this keyword deye access korte hobe.
    // class e dui dhoroner property thake- (1) static; (2) non-static;


?>