<?php
    namespace cal;
    include('vendor/autoload.php');
    use cal\Calculator;
?>


<div class="container d-flex justify-content-center align-items-center">
    <div class="text-center bg-success text-white p-4 mt-2 "> Result is : <?php  echo  $output ?? '' ?></div>
</div>