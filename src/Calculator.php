<?php

    namespace cal;


    class Calculator{
        public function sum($number1,$number2)
        {
            $result = $number1+$number2;
            return $result;
        }

        public function sub($number1,$number2)
        {
            $result = $number1-$number2;
            return $result;
        }

        public function mul($number1,$number2)
        {
            $result = $number1*$number2;
            return $result;
        }

        public function div($number1,$number2)
        {
            $result = $number1/$number2;
            return $result;
        }

        public function mod($number1,$number2)
        {
            $result = $number1%$number2;
            return $result;
        }
    }





?>